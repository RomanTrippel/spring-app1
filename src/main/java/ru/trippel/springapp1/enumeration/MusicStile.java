package ru.trippel.springapp1.enumeration;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum MusicStile {

    @NotNull
    JAZZ_MUSIC("Jazz music"),
    @NotNull
    ROCK_MUSIC("Rock music");

    @Getter
    @NotNull
    private String displayName = "";

    MusicStile(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
