package ru.trippel.springapp1;


import com.sun.istack.internal.NotNull;
import ru.trippel.springapp1.context.Bootstrap;

public class Application {

    public static void main(String[] args) throws Exception {
        @NotNull
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }


}
