package ru.trippel.springapp1.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.trippel.springapp1.SpringConfig;
import ru.trippel.springapp1.api.IPlayerService;
import ru.trippel.springapp1.api.IServiceLocator;
import ru.trippel.springapp1.api.IStateService;
import ru.trippel.springapp1.api.ITerminalService;
import ru.trippel.springapp1.command.AbstractCommand;
import ru.trippel.springapp1.service.PlayerService;
import ru.trippel.springapp1.service.StateService;
import ru.trippel.springapp1.service.TerminalService;
import ru.trippel.springapp1.util.PackageClassFinderUtil;
import ru.trippel.springapp1.view.BSView;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Getter
@NoArgsConstructor
public class Bootstrap implements IServiceLocator {

    @NotNull
    private final IServiceLocator serviceLocator = this;


    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();

    @NotNull
    private final IStateService stateService = new StateService(commands);

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final IPlayerService playerService = new PlayerService(this);

    @NotNull
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

    public void start() throws Exception {
        BSView.printWelcome();
        registerCommands();
        @NotNull String command = "";
        while (true) {
            try {
                command = terminalService.read();
                if ("EXIT".equals(command)) {
                    BSView.printGoodbye();
                    break;
                }
                if (command.isEmpty()) {
                    BSView.printErrorCommand();
                    continue;
                }
                getCommand(command).execute();
            } catch (IOException e) {
                BSView.printErrorCommand();
                e.printStackTrace();
            }
        }
    }

    @Nullable
    private AbstractCommand getCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return null;
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) BSView.printIncorrectlyData();
        return abstractCommand;
    }

    private void registerCommands() throws IllegalAccessException, InstantiationException {
        @NotNull AbstractCommand command;
        @NotNull final String pathCommandPackage = "ru.trippel.springapp1.command";
        @NotNull final Set<Class<? extends AbstractCommand>> allClasses = PackageClassFinderUtil.
                getAllClasses(pathCommandPackage);
        for (@NotNull final Class<? extends AbstractCommand> abstractCommand: allClasses) {
            command = abstractCommand.newInstance();
            command.setServiceLocator(serviceLocator);
            commands.put(command.getNameCommand(), command);
        }
    }

}
