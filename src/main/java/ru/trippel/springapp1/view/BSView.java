package ru.trippel.springapp1.view;

public class BSView {

    public static void printWelcome(){
        System.out.println("~~~~~Welcome to Music Player~~~~~" +
                "\nEnter a command. For help and viewing the available commands, type \"help\"." +
                "\nFor Exit, type \"exit\".");
    }

    public static void printGoodbye(){
        System.out.println("~~~~~See you soon. Come again!~~~~~");
    }

    public static void printIncorrectlyData() {
        System.out.println("Data entered incorrectly.");
    }

    public static void printErrorCommand() {
        System.out.println("Invalid command.");
    }

}
