package ru.trippel.springapp1.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.springapp1.api.IStateService;
import ru.trippel.springapp1.command.AbstractCommand;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class StateService implements IStateService {

    @NotNull
    private final Map<String, AbstractCommand> commands;

    @NotNull
    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

}
