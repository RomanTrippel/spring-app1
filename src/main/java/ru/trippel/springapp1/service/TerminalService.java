package ru.trippel.springapp1.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.springapp1.api.ITerminalService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@NoArgsConstructor
public final class TerminalService implements ITerminalService {

    @NotNull
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    public final String read() throws IOException {
        return reader.readLine();
    }

}