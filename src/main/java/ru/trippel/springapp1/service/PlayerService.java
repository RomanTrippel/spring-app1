package ru.trippel.springapp1.service;

import org.jetbrains.annotations.NotNull;
import ru.trippel.springapp1.api.IPlayerService;
import ru.trippel.springapp1.context.Bootstrap;
import ru.trippel.springapp1.entity.MusicPlayer;
import ru.trippel.springapp1.entity.MusicPlayerPrototype;

import java.util.List;

public class PlayerService implements IPlayerService {

    @NotNull
    private final Bootstrap bootstrap;

    public PlayerService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void playRandom(int numberGenre) {
        @NotNull
        final MusicPlayer musicPlayer = bootstrap.getContext().getBean("musicPlayer", MusicPlayer.class);
        musicPlayer.setGenre(numberGenre);
        List<String> songList = musicPlayer.getList();
        final int random = (int) (Math.random() * 3);
        System.out.println(songList.get(random));
    }

    @Override
    public void test() {
        final MusicPlayer musicPlayer = bootstrap.getContext().getBean("musicPlayer", MusicPlayer.class);
        System.out.println(musicPlayer.getName());
        System.out.println(musicPlayer.getVolume());

        final MusicPlayer musicPlayerSingleton = bootstrap.getContext().getBean("musicPlayer", MusicPlayer.class);
        final MusicPlayer musicPlayerSingleton1 = bootstrap.getContext().getBean("musicPlayer", MusicPlayer.class);
        if (musicPlayerSingleton == musicPlayerSingleton1) {
            System.out.println("It's a Singleton");
        } else {
            System.out.println("It's a Prototype");
        }
        final MusicPlayerPrototype musicPlayerPrototype = bootstrap.getContext().getBean(
                "musicPlayerPrototype", MusicPlayerPrototype.class);
        final MusicPlayerPrototype musicPlayerPrototype1 = bootstrap.getContext().getBean(
                "musicPlayerPrototype", MusicPlayerPrototype.class);
        if (musicPlayerPrototype1 == musicPlayerPrototype) {
            System.out.println("It's a Singleton");
        } else {
            System.out.println("It's a Prototype");
        }


    }

}
