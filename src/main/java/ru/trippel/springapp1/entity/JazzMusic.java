package ru.trippel.springapp1.entity;

import org.springframework.stereotype.Component;

import java.util.List;

public class JazzMusic extends AbstractMusic{

    public JazzMusic() {
        getListSong().add("Jazz song #1");
        getListSong().add("Jazz song #2");
        getListSong().add("Jazz song #3");
    }

    @Override
    public void destroy() {
        System.out.println("Destroy-method in JazzMusic because it's singleton");
    }

}
