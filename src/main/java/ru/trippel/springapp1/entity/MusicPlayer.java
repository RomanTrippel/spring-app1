package ru.trippel.springapp1.entity;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

public class MusicPlayer {

    @Getter
    @Value("${volume}")
    private int volume;

    @Getter
    @Value("${name}")
    private String name;

    private int genre = 1;

    @Nullable
    private List<AbstractMusic> musicList;


    public MusicPlayer(@NotNull final List<AbstractMusic> musicList) {
        this.musicList = musicList;
    }

    @Nullable
    public List<String> getList() {
        List<String> result = null;
        result = musicList.get(genre-1).getListSong();
        return result;
    }

    public void setGenre(int numberGenre) {
        genre = numberGenre;
    }
}
