package ru.trippel.springapp1.entity;

public class RockMusic extends AbstractMusic{

    public RockMusic() {
            getListSong().add("Rock song #1");
            getListSong().add("Rock song #2");
            getListSong().add("Rock song #3");
    }

    @Override
    public void destroy() {
        System.out.println("Destroy-method in RockMusic");
    }

}
