package ru.trippel.springapp1.entity;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractMusic {

    @NotNull
    private List<String> listSong = new ArrayList<>();

    @PostConstruct
    public void init(){
        System.out.println("Started init method");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("Started destroy method");
    }

}
