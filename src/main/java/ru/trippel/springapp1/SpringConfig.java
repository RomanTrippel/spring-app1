package ru.trippel.springapp1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import ru.trippel.springapp1.entity.*;

import java.util.Arrays;
import java.util.List;

@Configuration
@PropertySource("classpath:musicPlayer.properties")
public class SpringConfig {

    @Bean
    public JazzMusic jazzMusic () {
        return new JazzMusic();
    }

    @Bean
    public RockMusic rockMusic () {
        return new RockMusic();
    }

    @Bean
    public List<AbstractMusic> musicList(){
        return Arrays.asList(jazzMusic(), rockMusic());
    }

    @Bean
    @Scope("singleton")
    public MusicPlayer musicPlayer () {
        return new MusicPlayer(musicList());
    }

    @Bean
    @Scope("prototype")
    public MusicPlayerPrototype musicPlayerPrototype () {
        return new MusicPlayerPrototype(musicList());
    }

}
