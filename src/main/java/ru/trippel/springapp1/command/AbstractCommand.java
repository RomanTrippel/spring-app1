package ru.trippel.springapp1.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trippel.springapp1.api.IServiceLocator;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    public abstract String getNameCommand();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

}

