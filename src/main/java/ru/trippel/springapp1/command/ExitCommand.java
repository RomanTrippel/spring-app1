package ru.trippel.springapp1.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.springapp1.context.Bootstrap;
import ru.trippel.springapp1.view.BSView;

@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit of the application.";
    }

    @Override
    public void execute() {
        ((Bootstrap) serviceLocator).getContext().close();
        BSView.printGoodbye();
        System.exit(0);
    }

}
