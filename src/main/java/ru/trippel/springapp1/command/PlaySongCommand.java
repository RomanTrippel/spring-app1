package ru.trippel.springapp1.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.springapp1.entity.JazzMusic;
import ru.trippel.springapp1.service.PlayerService;
import ru.trippel.springapp1.service.TerminalService;
import ru.trippel.springapp1.view.BSView;

import java.io.IOException;

import static ru.trippel.springapp1.enumeration.MusicStile.JAZZ_MUSIC;
import static ru.trippel.springapp1.enumeration.MusicStile.ROCK_MUSIC;

@NoArgsConstructor
public final class PlaySongCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "play song";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Playing a song.";
    }

    @Override
    public void execute() throws IOException {
        int numberGenre;
        System.out.println("Choose a genre of music:");
        System.out.println("1. " + JAZZ_MUSIC.getDisplayName());
        System.out.println("2. " + ROCK_MUSIC.getDisplayName());
        System.out.println("Enter 1 or 2.");
        numberGenre = Integer.parseInt(serviceLocator.getTerminalService().read());
        serviceLocator.getPlayerService().playRandom(numberGenre);
    }

}
