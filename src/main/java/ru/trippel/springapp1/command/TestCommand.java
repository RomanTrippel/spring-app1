package ru.trippel.springapp1.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import static ru.trippel.springapp1.enumeration.MusicStile.JAZZ_MUSIC;
import static ru.trippel.springapp1.enumeration.MusicStile.ROCK_MUSIC;

@NoArgsConstructor
public final class TestCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "test";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Produces a test of the issue under study.";
    }

    @Override
    public void execute() throws IOException {
        serviceLocator.getPlayerService().test();
    }

}
