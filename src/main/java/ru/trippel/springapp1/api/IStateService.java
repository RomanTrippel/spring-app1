package ru.trippel.springapp1.api;

import org.jetbrains.annotations.NotNull;
import ru.trippel.springapp1.command.AbstractCommand;
import java.util.List;

public interface IStateService {

    @NotNull
    List<AbstractCommand> getCommands();

}
