package ru.trippel.springapp1.api;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    IStateService getStateService();

    @NotNull
    IPlayerService getPlayerService();

}
