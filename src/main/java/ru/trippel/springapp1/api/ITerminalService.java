package ru.trippel.springapp1.api;

import org.jetbrains.annotations.NotNull;
import java.io.IOException;

public interface ITerminalService {
    @NotNull
    String read() throws IOException;
}
